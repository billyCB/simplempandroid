package com.caballero.billy.encuesta.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.caballero.billy.encuesta.constract.FormEncuestaContract.FormEntry;
import com.caballero.billy.encuesta.models.Encuesta;
import com.caballero.billy.encuesta.util.Constants;
import com.caballero.billy.encuesta.util.MySQLiteHelper;

import java.util.ArrayList;

/**
 * Created by Billy on 21/11/2015.
 */
public class EncuestaDAO {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    public EncuestaDAO(Context context) {
        this.dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        this.database = dbHelper.getWritableDatabase();
    }

    public void close(){
        dbHelper.close();
    }

    public long insertar(String nombre, String apellido, String dni, String email, String candidatoOption){
        long state = Constants.BAD_STATE;
        try{
            ContentValues values = new ContentValues();
            values.put(FormEntry.COLUMN_NAME_FIRST_NAME, nombre);
            values.put(FormEntry.COLUMN_NAME_LAST_NAME, apellido);
            values.put(FormEntry.COLUMN_NAME_DNI,dni);
            values.put(FormEntry.COLUMN_NAME_EMAIL,email);
            values.put(FormEntry.COLUMN_NAME_CANDIDATO_OPTION,candidatoOption);
            state = this.database.insert(FormEntry.TABLE_NAME, FormEntry.COLUMN_NAME_NULLABLE, values);
        }catch (Exception e){
            state = Constants.BAD_STATE;
        }
        return state;
    }

    public ArrayList<Encuesta> listadoGeneral(){
        Cursor c;
        ArrayList<Encuesta> listEncuesta = new ArrayList<Encuesta>();
        c = this.database.rawQuery("SELECT * FROM " + FormEntry.TABLE_NAME, null);
        while (c.moveToNext()){
            Encuesta encuesta = new Encuesta();
            encuesta.setFirstName(c.getString(c.getColumnIndexOrThrow(FormEntry.COLUMN_NAME_FIRST_NAME)));
            encuesta.setLastName(c.getString(c.getColumnIndexOrThrow(FormEntry.COLUMN_NAME_LAST_NAME)));
            encuesta.setDni(c.getString(c.getColumnIndexOrThrow(FormEntry.COLUMN_NAME_DNI)));
            encuesta.setEmail(c.getString(c.getColumnIndexOrThrow(FormEntry.COLUMN_NAME_EMAIL)));
            encuesta.setCandidatoOption(c.getString(c.getColumnIndexOrThrow(FormEntry.COLUMN_NAME_CANDIDATO_OPTION)));
            listEncuesta.add( encuesta);
        }
        c.close();
        return listEncuesta;
    }
}
