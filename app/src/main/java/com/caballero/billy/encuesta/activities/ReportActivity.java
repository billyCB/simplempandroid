package com.caballero.billy.encuesta.activities;

import android.database.SQLException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.caballero.billy.encuesta.DAO.EncuestaDAO;
import com.caballero.billy.encuesta.R;
import com.caballero.billy.encuesta.models.Encuesta;
import com.caballero.billy.encuesta.util.Constants;
import com.caballero.billy.encuesta.util.Util;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;

public class ReportActivity extends AppCompatActivity {
    public static final String TAG = ReportActivity.class.getSimpleName();
    public String[] candidatos;
    private ArrayList<Encuesta> encuestas;
    private EncuestaDAO encuestaDAO;
    private PieChart mChart;
    private FrameLayout containerChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        initElements();
    }

    private void initElements(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.containerChart = (FrameLayout) findViewById(R.id.chart);
        this.mChart = new PieChart(this);
        this.candidatos = getResources().getStringArray(R.array.candidatos);
        initDAO();
        initChart();
    }

    private void initChart(){
        this.containerChart.addView(this.mChart);
        this.containerChart.setBackgroundColor(getResources().getColor(R.color.white));
        this.mChart.setCenterText("");
        this.mChart.setCenterTextSize(10f);
        this.mChart.setHoleRadius(50f);
        this.mChart.setTransparentCircleRadius(55f);
        this.mChart.setUsePercentValues(true);
        this.mChart.setDescription("");
        this.mChart.setNoDataText("");
        this.mChart.setDescription(Constants.CHART_DESCRIPTION);


        Legend l = this.mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_RIGHT);
        this.mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);

        //add info
        drawChart();
    }

    private void initDAO(){
        this.encuestaDAO = new EncuestaDAO(this);
        try{
            this.encuestaDAO.open();
            getDataFromDataBase();
        }catch (SQLException e){
            Log.v(TAG, e.toString());
        }
    }

    private void getDataFromDataBase(){
        this.encuestas= new ArrayList<Encuesta>();
        this.encuestas = this.encuestaDAO.listadoGeneral();
    }

    private float getTotalFromEncuestas(String option){
        int total = 0;
        for (Encuesta encuesta : this.encuestas)
            if ( Integer.parseInt( encuesta.getCandidatoOption()) == Integer.parseInt( option) )
                total++;

        int totalEncuestas = this.encuestas.size();
        double percent = (100*total)/totalEncuestas;
        Log.v(TAG,"" + percent);
        return (float)percent;
    }

    private void drawChart(){
        PieDataSet dataSet = new PieDataSet(getTotalsFromEncuesta(), "Total");
        dataSet.setSliceSpace(1f);
        dataSet.setSelectionShift(5f);

        dataSet.setColors(Util.getListColors());

        PieData data = new PieData(candidatos, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(getResources().getColor(R.color.colorPrimaryDark));
        this.mChart.setData(data);
        this.mChart.invalidate();
    }

    private ArrayList<Entry> getTotalsFromEncuesta(){
        ArrayList<Entry> entries = new ArrayList<Entry>();
        entries.add(new Entry(getTotalFromEncuestas("0"), 0));
        entries.add(new Entry(getTotalFromEncuestas("1"), 1));
        entries.add(new Entry(getTotalFromEncuestas("2"), 2));
        entries.add(new Entry(getTotalFromEncuestas("3"), 3));
        entries.add(new Entry(getTotalFromEncuestas("4"), 4));
        return entries;
    }


}
