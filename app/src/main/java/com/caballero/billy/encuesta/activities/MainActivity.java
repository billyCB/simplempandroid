package com.caballero.billy.encuesta.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.caballero.billy.encuesta.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mBtRegister;
    private Button mBtReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
    }

    private void setupView(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mBtRegister = (Button)findViewById(R.id.bt_register);
        mBtReport = (Button)findViewById(R.id.bt_report);
        mBtRegister.setOnClickListener(this);
        mBtReport.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_register:
                goToRegisterActivity();
                break;
            case R.id.bt_report:
                goToReportActivity();
                break;
        }
    }

    private void goToRegisterActivity(){
        Intent intent = new Intent(this, FormActivity.class);
        startActivity(intent);
    }

    private void goToReportActivity(){
        Intent intent = new Intent(this, ReportActivity.class);
        startActivity(intent);
    }
}
