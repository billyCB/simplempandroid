package com.caballero.billy.encuesta.models;

/**
 * Created by Billy on 21/11/2015.
 */
public class Encuesta {

    private String firstName;
    private String lastName;
    private String dni;
    private String email;
    private String candidatoOption;

    public String getCandidatoOption() {
        return candidatoOption;
    }

    public void setCandidatoOption(String candidatoOption) {
        this.candidatoOption = candidatoOption;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
