package com.caballero.billy.encuesta.constract;

import android.provider.BaseColumns;

/**
 * Created by Billy on 21/11/2015.
 */
public class FormEncuestaContract {

    public FormEncuestaContract() {
    }
    public static abstract class FormEntry  implements BaseColumns{
        public static final String TABLE_NAME = "encuesta";
        public static final String COLUMN_NAME_FIRST_NAME = "first_name";
        public static final String COLUMN_NAME_LAST_NAME = "last_name";
        public static final String COLUMN_NAME_DNI = "dni";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_CANDIDATO_OPTION = "candidato_option";
        public static final String COLUMN_NAME_NULLABLE = null;
    }
}
