package com.caballero.billy.encuesta.util;

/**
 * Created by Billy on 21/11/2015.
 */
public class Constants {

    public static final String DATA_BASE_NAME = "encuesta.utp.edu.billy";
    public static final int DATA_BASE_VERSION = 1;
    public static final int BAD_STATE = 0;

    public static final String CHART_DESCRIPTION = "Analisis de candidatos";
}
