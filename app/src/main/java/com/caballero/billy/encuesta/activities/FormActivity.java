package com.caballero.billy.encuesta.activities;

import android.database.SQLException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.caballero.billy.encuesta.DAO.EncuestaDAO;
import com.caballero.billy.encuesta.R;
import com.caballero.billy.encuesta.util.Constants;

public class FormActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = FormActivity.class.getSimpleName();
    private EditText mEtFirstName;
    private EditText mEtLastName;
    private EditText mEtDNI;
    private EditText mEtEmail;
    private Button mBtRegister;
    private RadioGroup mGroupOption;
    private EncuestaDAO encuestaDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        setupView();
        initElements();
    }

    private void setupView(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mEtFirstName = (EditText) findViewById(R.id.et_first_name);
        mEtLastName = (EditText) findViewById(R.id.et_last_name);
        mEtDNI = (EditText) findViewById(R.id.et_dni);
        mEtEmail = (EditText) findViewById(R.id.et_email);
        mBtRegister = (Button) findViewById(R.id.bt_register);
        mGroupOption = (RadioGroup) findViewById(R.id.rg_option);
        mBtRegister.setOnClickListener(this);
    }

    private void initElements(){
        encuestaDAO = new EncuestaDAO(this);
        try{
            encuestaDAO.open();
        }catch (SQLException e){
            Log.v(TAG, e.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.bt_register:
                registerEncuesta();
                break;
        }
    }

    private void registerEncuesta(){
        String fistName = mEtFirstName.getText().toString();
        String lastName = mEtLastName.getText().toString();
        String dni = mEtDNI.getText().toString();
        String email = mEtEmail.getText().toString();
        int optionIndex = getElementOfOptions();

        long state = Constants.BAD_STATE;
        state = encuestaDAO.insertar(fistName, lastName, dni, email, String.valueOf( optionIndex));
        if (state == Constants.BAD_STATE){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_message_fail), Toast.LENGTH_LONG).show();
        }else{
            finish();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_message_succesfull), Toast.LENGTH_LONG).show();
        }
    }

    private int getElementOfOptions(){
        int radioButtonID = mGroupOption.getCheckedRadioButtonId();
        View radioButton = mGroupOption.findViewById(radioButtonID);
        int idx = mGroupOption.indexOfChild(radioButton);
        return idx;
    }
}
